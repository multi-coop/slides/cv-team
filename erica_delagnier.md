# Erica Delagnier


![](images/delagnier.jpg)

## Développeuse

- Ingénieure de l'ENSEEIHT
- 9 ans d'expérience dans les produits numériques

## Compétences

## Exemples de réalisations

- Participation au lancement et à la construction de [Plume](https://eig.etalab.gouv.fr/defis/plume/) à la Cour des comptes de 2019 à 2020
- Architecte logicielle de [Decalog](https://www.decalog.net/) de 2016 à 2018
- Chef de projet informatique [Telegrafik](https://www.telegrafik.fr/) de 2014 à 2016

## Stack technique

