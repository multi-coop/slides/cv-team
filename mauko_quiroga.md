# Mauko Quiroga


![ ](images/mauko.png)


## Product manager secteur public

- Entrepreneur du numérique
- Alumni Etalab & beta.gouv.fr
- 8 ans d’expérience dans le numérique public

## Compétences

## Exemples de réalisations

- Travaux préparatoires et lancement de la première version du [Portail national des données ouvertes de transport](https://transport.data.gouv.fr/)
- Mentor et gestion partenariale institutionnelle dans le cadre du projet [LexImpact](leximpact.an.fr) 
- Chef produit du commun numérique [OpenFisca](https://openfisca.org/en/)


## Stack technique