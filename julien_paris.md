# Julien Paris

## Développeur / _product manager_

![](images/team/julien-paris.jpg)


Après une formation initiale d'architecte DPLG, puis avoir mené des projets culturels dans la coopération culturelle internationale (ambassade d'Espagne au Mexique, ambassade de France en Egypte), fait de la recherche (doctorant CNRS en Turquie), je me suis finalement reconverti dans le numérique en tant que développeur _fullstack_.

Je développe uniquement des logiciels ouverts et je milite ainsi de cette manière pour soutenir le **mouvement de l'open data et du logiciel libre**. Je m'intéresse en particulier à la _data visualisation_ et aux processus de contribution aux données ouvertes.

Depuis 2015 j'ai **dirigé et développé des projets numériques** pour des institutions ministérielles (ANCT, Bercy, CGET, Agence Bio), inter-ministérielles (DINUM), des associations et think tanks acteurs de l'intérêt général (Mednum, PiNG, Ternum, Rhinocc, Décider Ensemble), et pour d'autres structures publiques comme une bibliothèque à Nantes.

J'ai participé au programme "Entrepreneur d'Intérêt Général" d'Etalab en 2018. Je rejoins Johan Richer en 2021 pour amorcer la transformation de Jailbreak en coopérative, qui devient alors multi en 2022.

## Compétences

## Exemples de réalisations

## Stack technique
