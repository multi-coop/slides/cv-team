# Pierre Camilleri

![ ](images/camilleri.jpg)


## Data Scientist

- Ingénieur de l’École polytechnique
- Docteur dans le domaine du transport de marchandises (sous la direction de 
Lætitia Dablanc)
- Associé co-fondateur de la coopérative multi


## Compétences

## Exemples de réalisations

Participation au lancement et à la construction :

- de [Signaux faibles](https://beta.gouv.fr/startups/signaux-faibles.html) de 
2018 de 2020
- du service [Transition Écologique des Entreprises](https://mission-transition-ecologique.beta.gouv.fr/), à 
l’incubateur de l’ADEME, depuis juillet 2023


## Stack technique
