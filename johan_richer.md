# Johan Richer

## Expert open data / _product manager_

![](images/richer.jpg)

Product owner et coach agile

Alumni Etalab
Président et co-fondateur de multi



Issu d’une formation en sociologie politique et relations internationales et après un passage au ministère des affaires étrangères, Johan commence sa carrière dans le numérique participant à différentes initiatives de cartographie collaborative à vocation humanitaire en France et à l’international : CartONG, MapFugees, Missing Maps…

Au sein d’Etalab, l'agence française chargée de l’ouverture des données publiques, il travaille sur les sujets de gouvernement ouvert et à la création d’une « boîte à outils numériques ».

Il fonde Jailbreak en 2017 avec l’ambition de mettre le logiciel libre, l’open data, le gouvernement ouvert et les méthodes agiles au service de l'intérêt général. Jailbreak deviendra multi avec le passage en SCOP en 2022.

Johan est également co-fondateur et trésorier de Code for France.

## Compétences

## Exemples de réalisations


Exemples de réalisations :

- Product Owner de [dialog.beta.gouv.fr](https://dialog.beta.gouv.fr/), plateforme visant à faciliter l'intégration de la réglementation poids lourds dans les GPS routiers
- Product owner de GDR Santé pour l'ANSM (évaluation des risques des dossiers d'AMM)
- Product owner de [catalogue.data.gouv.fr](https://catalogue.data.gouv.fr/), développé pour permettre aux ministères de créer, gérer et ouvrir leurs catalogues de données
- Product owner de [Validata](https://validata.fr/), service de validation de données au service de la qualité de l'_open data_


## Stack technique
