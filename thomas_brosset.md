# Thomas Brosset


![ ](images/thomas.jpg)


## Consultant solutions numériques d'intérêt général / Lead Tech

- 12 ans d'expérience
- Co-fondateur de la coopérative multi
- Alumni du programme Entrepreneur d'Intérêt Général (EIG, promotion 2021)
- Diplômé d'un master 2 Urbanisme et Aménagement urbain (Institut d'Urbanisme de Lyon)

:::
::::::

Diplômé d’un master Urbanisme et Aménagement urbain, Thomas Brosset travaille depuis plus de 12 ans pour les collectivités locales et le secteur public sur des sujets autour de la cartographie, de l’open data, et du développement d’outils numériques.
Il a travaillé sur la modélisation des documents d’urbanisme en 3D, l’identification des risques de pollution à partir des données Basias/Basol et ICPE, le repérage des friches urbaines, et l’observation des prix fonciers. Il a développé la plateforme de travail collaboratif spreadsheep.io pour permettre aux petites équipes de mieux s’organiser et de créer leurs propres outils métiers.

## Compétences

- stratégies territoriales
- développement de services numériques
- traitement de données spatiales et cartographie
- opendata

## Exemples de réalisations

Développement de services numériques pour l'intérêt général :

- EPA Sénart : mise à disposition d’une plateforme de cartographie numérique
- OpenDataFrance : investigation sur un prototype d’outil collaboratif autour de la donnée et de l’open data
- ADEME : développement d’un modèle de données pour les Inventaires Historiques Urbains
- [ADEX](https://gitlab.has-sante.fr/has-sante/public/adex) : exploitation des données de Transparence Santé pour la Haute Autorité de Santé (#python #elastic #react/TS)
- [MonitorEnv](https://github.com/MTES-MCT/monitorenv):  outil métier pour le CACEM - Ministère de la Transition Écologique (#cartographie #Kotlin #React/TS #python)

## Stack technique

- Python
- TypeScript
- React
- Kotlin
- Postgres/PostGIS
- Ruby on Rails
