---
title: Les membres de l'équipe multi
subtitle: CV et présentation des membres de l'équipe
author: multi.coop
date: 22 janvier 2024
title-slide-attributes:
    data-background-image: "static/logo.svg, static/logo_client.svg"
    data-background-size: "auto 10%, auto 12%"
    data-background-position: "95% 95%, 70% 97%"
---

# Présentation de multi
